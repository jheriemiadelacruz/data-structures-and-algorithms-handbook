# Links and Resources

- [Git by Traversy Media](https://www.youtube.com/watch?v=SWYqp7iY_Tc)
- [Git Cheat Sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)
- [Markdown by Traversy Media](https://www.youtube.com/watch?v=HUBNt18RFbo)
- [Markdown Cheat Sheet](https://www.markdownguide.org/cheat-sheet/)
- [Practical Algorithms and Data Structures](https://bradfieldcs.com/algos/)
- [Open Data Structures - Python](https://opendatastructures.org/ods-python.pdf)
- [Data Structures and Algorithms in Python](/files/data-structures-and-algorithms-in-python.pdf)
- [Learn Python in 1 Hour](https://www.youtube.com/watch?v=kqtD5dpn9C8)
- [Learn Python in 5 Minutes](https://www.pythoncheatsheet.org/cheatsheet/basics)
- [Visualgo](https://visualgo.net/en)
