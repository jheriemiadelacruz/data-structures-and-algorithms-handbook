# Notes

Here we list our "Aha"s!

- FIFO
- FILO
- Constant runtime O(1)
- Logarithmic runtime O(log n)
- Linear runtime O(n)
- Quadratic runtime O(n^2)
- Quasilinear runtime O(n log n)
- Cubic runtime O(n^3)
- Factorial runtime O(n!)
- Exponential runtime O(x^n)
- Polynomial runtime O(n^x)
- Brute force technique
- Traveling salesman
- Recursion
- Interface
- Best, average, and worst-case scenarios
- We measure efficiency by its worst-case scenario
- Time complexity is how fast or slow a task is completed
- Space complexity is how many resources we need to complete a task
- The growth rate is when we plotted the inputs and see how the algorithm will perform
- Big O notation is the theoretical definition of the complexity of an algorithm as a function of size

### Forking Project

1. Review [Forking Workflow](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html)
2. Visit [Data Structures and Algorithms Repository](https://gitlab.com/christianvisaya/data-structures-and-algorithms)
3. Click the "fork" button

![alt fork-project-1.png](/images/fork-project-1.png)
4. Change namespace, then click "Fork Project"
![alt fork-project-2.png](/images/fork-project-2.png)
5. You can now clone your project on your local machine
![alt fork-project-3.png](/images/fork-project-3.png)

### Installing Python

#### For Windows

1. Go to "Microsoft store"
2. Search for Python
3. Install Python

![alt install-python-windows.jpg](/images/install-python-windows.png)

#### For macOS

1. [Download this](https://www.python.org/ftp/python/3.10.7/python-3.10.7-macos11.pkg)
2. Follow installation guide
