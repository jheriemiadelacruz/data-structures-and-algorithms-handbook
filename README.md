# Introduction

Welcome to our Data Structures and Algorithms Handbook!

## Updating this handbook

### Pre-requisites

Some tips may require terminal shell access on macOS/Linux. Ensure that your environment is working and that you have cloned the [data-structures-and-algorithms-handbook](https://gitlab.com/christianvisaya/data-structures-and-algorithms-handbook) project, for example.

- [Git by Traversy Media](https://www.youtube.com/watch?v=SWYqp7iY_Tc)
- [Git Cheat Sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)
- [Markdown by Traversy Media](https://www.youtube.com/watch?v=HUBNt18RFbo)
- [Markdown Cheat Sheet](https://www.markdownguide.org/cheat-sheet/)

#### 1. Clone repository

```
git clone https://gitlab.com/christianvisaya/data-structures-and-algorithms-handbook.git

git clone git@gitlab.com:christianvisaya/data-structures-and-algorithms-handbook.git
```

Sync it. Ensure that you stash away local changes not yet committed.

```
cd data-structures-and-algorithms-handbook
git stash
git checkout main
git pull
```

#### 2. Install honkit

1. Install [Node.JS LTS Version](https://nodejs.org/en/)
2. Install Honkit with NPM

```
npm install
```

3. Preview and serve your book using:

```
npx honkit serve
```

#### 3. Modify existing document

This handbook uses Honkit and Markdown. To continue with editing, you may refer to [Honkit documentation](https://github.com/honkit/honkit)

#### 4. Commit changes

Add and commit your changes to a new branch; for example, the branch name is **bsis-2-visaya-christian**. Push it to GitLab, create a new merge request, add screenshots of how your pages look like, and assign your MR Reviewer to Christian Visaya.

### Task 1: Know yourself

1. Add your entry to the "Meet the Team" section
2. Alphabetical order of the first name
3. Add your "pang-malupitan" image to the "team/images" directory with a file name like bsis_2_visaya_christian in .jpg format, camel case
4. Follow the template for other fields; make sure to fill out all of them
5. Add your 16Personalities link
6. Commit, push, and create a merge request
7. Deadline is September 24 @ 7 am

### Task 2: Get the ship ready and search for the treasure

1. Clone your forked repository, see the guide in our notes section
2. `cd` to your newly cloned repository
3. Create 2 folders on the project root folder, named `search` and `sort`
4. On `search` folder, create 2 files, named `linear_search` and `binary_search`, file extension is up to you, we do recommend using Python
5. On `sort` folder, create 2 files, name `bubble_sort` and `selection_sort`, file extension is still up to you
6. See screenshot for reference

![](/images/task-2-1.png)

7. If you use other programming languages, and not Python, update the `README.md` file on how to run your script

#### Task 2 Guide

- For the linear search algorithm, create at least 2 implementations of it, just separate the function on the same file, and choose what you want to be used by default when running the script
- For search algorithms, the script should take 2 inputs that will be inputted via terminal, comma-separated numbers with no spaces, i.e. `1,2,3,4,5` ; and a number to be searched, i.e. `5`
- If the target number is nowhere to be found on the list, output a message of `Not found`
- If the target number was in the list, output the array index where the number was found
- For the binary search algorithm, if the list of numbers wasn't sorted, display `List not sorted`
- On both search algorithms, output how many attempts were made to find the number
- For sorting algorithms, the script should take 1 input, a comma-separated number with no spaces, like in search algorithms
- The script should output the sorted sequence and how many steps were taken for the task to be completed
- Once the task is done, exit the program
